﻿
/* 
    VBS.Ramnit HTML Cleaner
    (c) 2013 , Sajad KK <kksajad@gmail.com>

    Released under GPL v2 Licence
    -------------------------------- 
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Security.Permissions;

namespace VBS.Ramnit_HTML_Cleaner
{
    public partial  class frmMain : Form
    {
        // ----- Variables -----

        public List<string> infectedFiles;
        public int m_infectCount = 0;
        private int m_cleanCount = 0;
        public int m_fileCount = 0;
        private bool m_pause = true;
        Stopwatch watch = new Stopwatch();

        // ----- Synchronizing Delegates -----

        private delegate void TestFileSyncHandler(TestFileEventArgs e);
        private TestFileSyncHandler TestFile;

        private delegate void FoundInfoSyncHandler(FoundInfoEventArgs e);
        private FoundInfoSyncHandler FoundInfo;
        private FoundInfoSyncHandler CleanFoundInfo;

        private delegate void OnGetFileCountHandler(OnGetFileCountEventArgs e);
        private OnGetFileCountHandler OnGetFileCount;

        private delegate void ThreadEndedSyncHandler(ThreadEndedEventArgs e);
        private ThreadEndedSyncHandler ThreadEnded;
        private ThreadEndedSyncHandler CleanThreadEnded;

        public frmMain()
        {
            InitializeComponent();
            infectedFiles = new List<string>();

        }


        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                if (Path.GetDirectoryName(folderBrowser.SelectedPath) != null)
                {
                    txtPath.Text = folderBrowser.SelectedPath + Path.DirectorySeparatorChar;
                }
                else
                {
                    txtPath.Text = folderBrowser.SelectedPath;
                }
            }
        }

        

        private void btnScan_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(txtPath.Text))
            {
                MessageBox.Show("Select a valid path for scanning", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            ResetValues();
           
            if(Search.Start(txtPath.Text))
            {
                DisableButtons();
                progressBar.Style = ProgressBarStyle.Marquee;
                lblStatus.ForeColor = Color.Blue;
                lblStatus.Text = "Preparing files...";
            }
        }

        
        
        private void btnClean_Click(object sender, EventArgs e)
        {
            lblStatus.ForeColor = Color.Black;
            lblInfect.Visible = false;

            progressBar.Maximum = m_infectCount;

            if (Search.CleanHTML())
            {
                DisableButtons();
                btnStop.Enabled = false;
                btnPause.Enabled = false;
                lblInfect.Visible = false;
                lblFileCount.ForeColor = Color.Green;
            }
           
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            this.ThreadEnded += new ThreadEndedSyncHandler(this_ThreadEnded);
            this.CleanThreadEnded += new ThreadEndedSyncHandler(this_CleanThreadEnded);
            this.FoundInfo += new FoundInfoSyncHandler(this_FoundInfo);
            this.CleanFoundInfo += new FoundInfoSyncHandler(this_CleanFoundInfo);
            this.TestFile += new TestFileSyncHandler(this_TestFile);
            this.OnGetFileCount += new OnGetFileCountHandler(this_OnGetFileCount);

            Search.ThreadEnded += new Search.ThreadEndedEventHandler(Search_ThreadEnded);
            Search.CleanThreadEnded += new Search.ThreadEndedEventHandler(Search_CleanThreadEnded);
            Search.FoundInfo += new Search.FoundInfoEventHandler(Search_FoundInfo);
            Search.CleanFoundInfo += new Search.FoundInfoEventHandler(Search_CleanFoundInfo);
            Search.TestFile += new Search.TestFileEventHandler(Search_TestFile);
            Search.OnGetFileCount += new Search.OnGetFileCountEventHandler(Search_OnGetFileCount);
            
        }

        private void Search_CleanThreadEnded(ThreadEndedEventArgs e)
        {
            this.Invoke(CleanThreadEnded, new object[] { e });
        }

        private void this_CleanThreadEnded(ThreadEndedEventArgs e)
        {
            btnClean.Enabled = false;
            EnableButtons();
            lblStatus.ForeColor = Color.Green;
            lblStatus.Text = "All files are cleaned";
            lblInfect.Visible = false;
            
            MessageBox.Show("All files are fixed successfully", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
       
        }

        private void Search_CleanFoundInfo(FoundInfoEventArgs e)
        {
            this.Invoke(CleanFoundInfo, new object[] { e });
        }

        private void this_CleanFoundInfo(FoundInfoEventArgs e)
        {
            ++m_cleanCount; 
            lblStatus.Text = e.Info.FullName;
            lblFileCount.Text = "Cleaned : " + m_cleanCount + "/" + m_infectCount;
            progressBar.PerformStep();

        }

        private void Search_OnGetFileCount(OnGetFileCountEventArgs e)
        {
            this.Invoke(OnGetFileCount, new object[] { e });
        }

        private void this_TestFile(TestFileEventArgs e)
        {
            ++m_fileCount;
            lblStatus.Text = e.Info.FullName;
            lblFileCount.Text = "Tested : " + m_fileCount;
            progressBar.PerformStep();
        }

        private void this_OnGetFileCount(OnGetFileCountEventArgs e)
        {

            btnStop.Enabled = true;
            btnPause.Enabled = true;

            lblStatus.ForeColor = Color.Black;
            progressBar.Maximum = e.TotalFiles;
            progressBar.Step = 1;
            progressBar.Style = ProgressBarStyle.Blocks;
        }

        private void Search_TestFile(TestFileEventArgs e)
        {
            this.Invoke(TestFile, new object[] { e });
        }

        private void this_ThreadEnded(ThreadEndedEventArgs e)
        {
            EnableButtons();

            if (!e.Success)
            {
                MessageBox.Show(e.ErrorMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (m_infectCount > 0)
            {

                btnClean.Enabled = true;
                lblStatus.ForeColor = Color.Blue;
                lblStatus.Text = "Scanning completed, start cleaning now!";
                progressBar.Value = 0;
            }
            else
            {
                lblStatus.ForeColor = Color.Green;
                lblStatus.Text = "Dont worry, everything is clean!";

            }
            
        }

        private void this_FoundInfo(FoundInfoEventArgs e)
        {
            ++m_infectCount;

            lblStatus.Text = e.Info.FullName;
            lblInfect.Text = "Infected : " + m_infectCount;
        }

        private void Search_FoundInfo(FoundInfoEventArgs e)
        {
           this.Invoke(FoundInfo, new object[] { e });
        }

        private void Search_ThreadEnded(ThreadEndedEventArgs e)
        {
           this.Invoke(ThreadEnded, new object[] { e });
           
        }
        private void ResetValues()
        {
            m_fileCount = 0;
            m_infectCount = 0;
            m_cleanCount = 0;
            m_pause = true;
            btnPause.Text = "PAUSE";

            progressBar.Value = 0;
            lblInfect.Text = null;
            lblFileCount.ForeColor = Color.Black;
            lblFileCount.Text = null;
        }

        private void DisableButtons()
        {
            btnScan.Enabled = false;
            btnBrowse.Enabled = false;
            btnClean.Enabled = false;

            lblInfect.Visible = true;
            lblFileCount.Visible = true;
        }

        private void EnableButtons()
        {
            btnScan.Enabled = true;
            btnBrowse.Enabled = true;
            btnStop.Enabled = false;
            btnPause.Enabled = false;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            Search.PauseResume();
            if (m_pause)
            {
                btnPause.Text = "RESUME";
                m_pause = false;
            }
            else
            {
                btnPause.Text = "PAUSE";
                m_pause = true;
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            Search.Stop();
            progressBar.Value = 0;
            btnPause.Text = "PAUSE";
            m_pause = true;
        }

    }
    
}
