﻿
/* 
    VBS.Ramnit HTML Cleaner
    (c) 2013 , Sajad KK <kksajad@gmail.com>

    Released under GPL v2 Licence
    -------------------------------- 
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VBS.Ramnit_HTML_Cleaner
{
    class OnGetFileCountEventArgs
    {
        private int m_totalFiles;

        public OnGetFileCountEventArgs(int totalFiles)
        {
            m_totalFiles = totalFiles;
        }

        public int TotalFiles
        {
            get { return m_totalFiles; }
        }
    }
}
