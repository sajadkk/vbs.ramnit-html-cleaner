﻿
/* 
    VBS.Ramnit HTML Cleaner
    (c) 2013 , Sajad KK <kksajad@gmail.com>

    Released under GPL v2 Licence
    -------------------------------- 
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Threading;

namespace VBS.Ramnit_HTML_Cleaner
{
    class Search
    {
        // ----- Asynchronous Events -----

        public delegate void TestFileEventHandler(TestFileEventArgs e);
        public static event TestFileEventHandler TestFile;

        public delegate void FoundInfoEventHandler(FoundInfoEventArgs e);
        public static event FoundInfoEventHandler FoundInfo;
        public static event FoundInfoEventHandler CleanFoundInfo;

        public delegate void OnGetFileCountEventHandler(OnGetFileCountEventArgs e);
        public static event OnGetFileCountEventHandler OnGetFileCount;

        public delegate void ThreadEndedEventHandler(ThreadEndedEventArgs e);
        public static event ThreadEndedEventHandler ThreadEnded;
        public static event ThreadEndedEventHandler CleanThreadEnded;

        // ----- Variables -----

        private static Thread m_thread = null;
        private static Boolean m_stop = false;
        private static Boolean m_pause = false;
        private static string m_path;
        private static object m_threadLock = new object();

        private static List<string> files, infectedFiles;

        public static IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);
        
        // ----- WIN32 -----

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct WIN32_FIND_DATAW
        {
            public FileAttributes dwFileAttributes;
            internal System.Runtime.InteropServices.ComTypes.FILETIME ftCreationTime;
            internal System.Runtime.InteropServices.ComTypes.FILETIME ftLastAccessTime;
            internal System.Runtime.InteropServices.ComTypes.FILETIME ftLastWriteTime;
            public int nFileSizeHigh;
            public int nFileSizeLow;
            public int dwReserved0;
            public int dwReserved1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string cFileName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
            public string cAlternateFileName;
        };

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr FindFirstFileW(string lpFileName, out WIN32_FIND_DATAW lpFindFileData);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        public static extern bool FindNextFile(IntPtr hFindFile, out WIN32_FIND_DATAW lpFindFileData);

        [DllImport("kernel32.dll")]
        public static extern bool FindClose(IntPtr hFindFile);

        // ----- Public Methods -----

        public static Boolean Start(string path)
        {
            Boolean success = false;

            if (m_thread == null)
            {
                ResetAll();

                m_path = path;

                m_thread = new Thread(new ThreadStart(SearchThread));
                m_thread.Start();

                success = true;
            }

            return success;
        }

        public static Boolean CleanHTML()
        {
            Boolean success = false;

            if (m_thread == null)
            {
                m_thread = new Thread(new ThreadStart(CleanHTMLFile));
                m_thread.Start();

                success = true;
            }

            return success;
        }

        public static void Stop()
        {
            m_stop = true;
            if (m_pause)
            {

                m_pause = false;
                lock (m_threadLock)
                {
                    Monitor.Pulse(m_threadLock);
                }
            }
        }

        public static void PauseResume()
        {
            if (m_pause)
            {

                m_pause = false;
                lock (m_threadLock)
                {
                    Monitor.Pulse(m_threadLock);
                }
            }
            else
                m_pause = true;
        }

        // ----- Private Methods -----

        private static void SearchThread()
        {
            Boolean success = true;
            string errorMsg = null;
            files = new List<string>();
            
            if (Directory.Exists(m_path))
            {
               CountFiles(m_path);
          
               var html = from file in files.Where(s => s.EndsWith(".htm") | s.EndsWith(".html")) select file;
               files = html.ToList();

                //raise an event so that we can set maximum value for progressbar
               if (OnGetFileCount != null)
                {
                    OnGetFileCount(new OnGetFileCountEventArgs(files.Count));
                }

               FindInfectedHTML();
            }
            else
            {
                success = false;
                errorMsg = "The directory\r\n" + m_path + "\r\ndoes not exist.";
            }

            // Remember the thread has ended:
            m_thread = null;

            // Raise an event:
            if (ThreadEnded != null)
            {
                ThreadEnded(new ThreadEndedEventArgs(success, errorMsg));
            }
        }

        private static void FindInfectedHTML()
        {       
                FileSystemInfo f;
                infectedFiles = new List<string>();
                
                foreach (string file in files)
                {
                        lock (m_threadLock)
                        {
                            while (m_pause)
                              Monitor.Wait(m_threadLock);
                        }
                        if (m_stop) break;
                    
                    try
                    {
                        f = new FileInfo(file);

                        if (TestFile != null)
                        {
                            TestFile(new TestFileEventArgs(f));
                        }

                        if (isPatternFound(file) > 0)
                        {
                            infectedFiles.Add(file);
                            // We have found a infected file, so let's raise an event
                            if (FoundInfo != null)
                            {
                                FoundInfo(new FoundInfoEventArgs(f));
                            }
                        }
                    }
                    catch (FileNotFoundException e)
                    {
                        continue;
                    }
                }
        }

        private static void CleanHTMLFile()
        {
            FileSystemInfo f;
            foreach (string file in infectedFiles)
            {
               
                try
                {
                    StreamReader reader = new StreamReader(file);
                    string content = reader.ReadToEnd();
                    reader.Close();

                    var match = Regex.Match(content, "<SCRIPT Language=VBScript>", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.RightToLeft);

                    if (match.Success)
                    {
                        int start = match.Index;
                        string result = content.Substring(0, start);

                        StreamWriter writer = new StreamWriter(file);
                        writer.Write(result);
                        writer.Close();
                        
                        if (CleanFoundInfo != null)
                        {
                            f = new FileInfo(file);
                            CleanFoundInfo(new FoundInfoEventArgs(f));
                        }

                    }
                }
                catch (FileNotFoundException e)
                {
                    continue;
                }

            }

            m_thread = null;

            if (CleanThreadEnded != null)
            {
                CleanThreadEnded(new ThreadEndedEventArgs(true, ""));
            }

        }

        private static void CountFiles(string Path)
        {
            // Is it a file ?
            if (File.Exists(Path))
                  files.Add(Path);
            // Loop directory.
            else
            {
                WIN32_FIND_DATAW FindData;
                IntPtr hFind = INVALID_HANDLE_VALUE;

                try
                {
                    hFind = FindFirstFileW(Path + @"\*", out FindData);
                    if (hFind != INVALID_HANDLE_VALUE)
                    {
                        do
                        {
                            if (FindData.cFileName == "." || FindData.cFileName == "..")
                                continue;

                            if (Path.Length > 150) continue;

                            string FullPath = Path + (Path.EndsWith("\\") ? "" : "\\") + FindData.cFileName;

                            if ((FindData.dwFileAttributes & FileAttributes.Directory) != 0)
                            {
                                CountFiles(FullPath);
                            }
                            else
                            {
                                if (FindData.nFileSizeHigh > 2000) continue;
                                if (FindData.cFileName.Length < 100)
                                {

                                    files.Add(FullPath);

                                }

                            }

                        }
                        while (FindNextFile(hFind, out FindData));

                    }
                }
                finally
                {
                    if (hFind != INVALID_HANDLE_VALUE)
                        FindClose(hFind);
                }
            }

           
        }

        private static int isPatternFound(string file)
        {
            StreamReader reader = new StreamReader(file);
            string content = reader.ReadToEnd();
            reader.Close();

            var match = Regex.Match(content, "<SCRIPT Language=VBScript>", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.RightToLeft);

            if (match.Success)
            {
                int start = match.Index;
                string result = content.Substring(start);

                if (Regex.IsMatch(result, "4D5A90000300000004000000FFFF0000B8000000000000004", RegexOptions.Compiled))
                {
                    var svchost = Regex.Match(result, "DropFileName = \"svchost.exe\"", RegexOptions.Compiled);
                    var run = Regex.Match(result, "WSHshell.Run DropPath, 0", RegexOptions.Compiled | RegexOptions.RightToLeft);
                    if (svchost.Success == true && run.Success == true)
                        return 1;
                }
            }

            return 0;
        }

        private static void ResetAll()
        {
            m_thread = null;
            m_stop = false;
            m_path = null;

            files = null;
            infectedFiles = null;
        }
    }
}
