﻿
/* 
    VBS.Ramnit HTML file Generator for testing
    (c) 2013 , Sajad KK <kksajad@gmail.com>

    Released under GPL v2 Licence
    -------------------------------- 
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Plossum.CommandLine;

namespace testgen
{
    [CommandLineManager(ApplicationName = "testgen", Copyright = "Copyright (c) 2013, Sajad KK",
        Description = "VBS.Ramnit HTML file generator for testing", 
        EnabledOptionStyles=OptionStyles.Group | OptionStyles.LongUnix)]
    class Options
    {
        [CommandLineOption(Name = "h", Aliases = "help", Description = "Display help")]
        public bool Help
        {
            get { return m_help; }
            set { m_help = value; }
        }

        [CommandLineOption(Name = "c", Aliases = "count", Description = "Specify no.of test files to generate", MinOccurs=1)]
        public int FileCount
        {
            get { return m_fileCount; }
            set { m_fileCount = value; }
        }

        [CommandLineOption(Name = "o", Aliases = "output", Description = "Specify the output path",  MinOccurs = 1)]
        public string Output
        {
            get { return m_output; }
            set { m_output = value; }
        }

        private string m_output;
        private int m_fileCount;
        private bool m_help;

    }

    class Program
    {
       private static Options options = new Options();
       private static Thread thread;

        static int Main(string[] args)
        {
           
            CommandLineParser parser = new CommandLineParser(options);
            parser.Parse();

            if (options.Help)
            {
                Console.WriteLine("\n" + parser.UsageInfo.ApplicationName + " " + parser.UsageInfo.ApplicationVersion);
                Console.WriteLine(parser.UsageInfo.ApplicationCopyright);
                Console.WriteLine(parser.UsageInfo.ApplicationDescription);
                Console.WriteLine("\n" + parser.UsageInfo.GetOptionsAsString(78));
                return 0;
            }
            else if (parser.HasErrors)
            {
                Console.WriteLine("\n" + parser.UsageInfo.ApplicationName + " " + parser.UsageInfo.ApplicationVersion);
                Console.WriteLine(parser.UsageInfo.ApplicationCopyright);
                Console.WriteLine(parser.UsageInfo.ApplicationDescription);
                Console.WriteLine("\n" + parser.UsageInfo.GetErrorsAsString(78));
                Console.WriteLine("\n" + parser.UsageInfo.GetOptionsAsString(78));
                return -1;
            }

            try
            {
                if (Directory.Exists(Path.GetFullPath(options.Output)))
                {
                   thread = new Thread(new ThreadStart(GenerateThread));
                   thread.Start();
                }
                else
                {
                    throw new DirectoryNotFoundException("Specified path does not exist");
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (DirectoryNotFoundException e)
            {
                 Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return 0;
            
        }

        private static void GenerateThread()
        {
            Generate(options.FileCount, options.Output);
        }

        private static void Generate(int n, string outputPath)
        {
      
            if (File.Exists("VBS.txt"))
            {
                StreamReader reader = new StreamReader("VBS.txt");
                string vbscode = reader.ReadToEnd();
                reader.Close();

                if (!outputPath.EndsWith("\\"))
                    outputPath += "\\";
                
                Console.WriteLine("Starting...");
                try
                {
                    for (int i = 1; i <= n; i++)
                    {
                        StreamWriter writer = new StreamWriter(outputPath + "test_" + i + ".htm");
                        writer.WriteLine("<!DOCTYPE html>");
                        writer.WriteLine("<html>");
                        writer.WriteLine("<head><title>Ramnit Test File</title></head>");
                        writer.WriteLine("<body>Hello Ramnit!</body>");
                        writer.WriteLine("</html>");
                        writer.Write(vbscode);
                        writer.Close();
                        Console.Write("\rGenerating files {0}%   ", (100 * i) / n);
                    }
                    Console.WriteLine("\nTest files generated successfully");
                }
                catch (UnauthorizedAccessException e)
                {
                    Console.WriteLine(e.Message + "\n\nrun with admin privilege...");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            else
                throw new FileNotFoundException("Could not locate VBS.txt");
        }
    }

}
